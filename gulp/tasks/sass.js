module.exports = function() {
  $.gulp.task("sass", function() {
    return $.gulp
      .src("sass/style.scss")
      .pipe($.gp.plumber()) //отслеживание ошибок
      .pipe($.gp.sourcemaps.init())
      .pipe($.gp.sass()) // компиляция в css
      .pipe($.gp.autoprefixer())
      .pipe($.gp.csso()) // оптимизация кода
      .pipe($.gp.rename("style.min.css"))
      .pipe($.gp.sourcemaps.write(""))
      .pipe($.gulp.dest("build/css"))
      .pipe($.browserSync.stream());
  });
};
